import { fraction, divide, add, multiply } from "mathjs"

const toFraction = (e) => {
  const { s, n, d } = e
  if (s === -1 && d !== 1) return `-${n}/${d}`
  if (s === -1 && d === 1) return `-${n}`
  if (s === 1 && d === 1) return `${n}`
  if (s === 1 && d !== 1) return `${n}/${d}`
}

const solver = (first, second, indexMain) => {
  if (first.length !== second.length || !first || !second || indexMain > first.length) {
    return {
      error: 'Greška pri unosu'
    }
  }
  const firstFract = first.map((cell) => fraction(cell))
  const secondFract = second.map((cell) => fraction(cell))
  const multiplier = multiply(secondFract[indexMain], -1)
  const firstDiv = firstFract.map((cell) => divide(cell, firstFract[indexMain]))
  const solved = secondFract.map((cell, index) => {
    const factor = multiply(firstDiv[index], multiplier)
    return add(cell, factor)
  })
  const firstPrint = firstDiv.map((cell) => toFraction(cell))
  const solvedPrint = solved.map((cell) => toFraction(cell))
  const firstFractPrint = firstFract.map((cell) => toFraction(cell))
  const secondFractPrint = secondFract.map((cell) => toFraction(cell))

  return {
    first: firstFractPrint,
    second: secondFractPrint,
    firstDiv: firstPrint,
    multiplier: toFraction(multiplier),
    solved: solvedPrint
  }
}

export default solver
