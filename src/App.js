import "./App.scss"
import solver from "./algorithm"
import { useState } from "react"
import toast from "react-hot-toast"
import { Toaster } from "react-hot-toast"
import Solution from "./Solution"

function App() {
  const [vodeciRed, setVodeciRed] = useState([])
  const [redTransformacije, setRedTransformacije] = useState([])
  const [vodeciStupac, setVodeciStupac] = useState(0)
  const [result, setResult] = useState()
  const onChange = (e, setValue) => setValue(e.target.value.split(" "))
  const onSubmit = (e) => {
    e.preventDefault()
    const res = solver(vodeciRed, redTransformacije, vodeciStupac)
    console.log(res)
    if (res.error) {
      toast.error(res.error)
      return null
    }
    setResult(res)
    toast.success("Izračun uspješan")
  }

  return (
    <>
      <Toaster />
      <div className="app">
        <form onSubmit={onSubmit} id="form">
          <label>Vodeći red</label>
          <input
            required
            type="text"
            onChange={(e) => onChange(e, setVodeciRed)}
          />
          <label>Red transformacije</label>
          <input
            required
            type="text"
            onChange={(e) => onChange(e, setRedTransformacije)}
          />
          <label>Pozicija vodećeg stupca</label>
          <input
            required
            type="number"
            onChange={(e) => setVodeciStupac(parseInt(e.target.value) - 1)}
          />
          <button type="submit" className="primary-btn">
            Izračunaj
          </button>
          <button
            type="button"
            className="danger-btn"
            onClick={() => {
              setResult()
              document.getElementById("form").reset()
            }}
          >
            Izbriši
          </button>
        </form>
        <Solution result={result} />
      </div>
    </>
  )
}

export default App
