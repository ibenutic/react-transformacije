const Solution = props => {
    const {result} = props
    if(!result) return null
    const {first,firstDiv,multiplier,second,solved} = result

    return <> 
    <div className="infoWrap">
        <h2>Provjera</h2>
        <h3>Vodeći red</h3>
        <div className="red">
            {first?.map(broj => <span key={Math.random()} className="broj">{broj}</span>)}
        </div>
        <h3>Red transformacije</h3>
        <div className="red">
            {
                second?.map(broj => <span className="broj">{broj}</span>)
            }
        </div>
    </div>
    <div className="infoWrap">
        <h2>Rješenje</h2>
        <h3>Podijeljen vodeći red</h3>
        <div className="red">
            {firstDiv?.map(broj => <span className="broj" key={Math.random()}>{broj}</span>)}
        </div>
        <h3>Transformirani red</h3>
        <div className="red">
            {solved?.map(broj => <span className="broj" key={Math.random()}>{broj}</span>)}
        </div>
        <h3>Koeficijent</h3>
        <p style={{width: 'fit-content'}} className="broj">{multiplier}</p>
    </div>
    </>
}

export default Solution